<?php

namespace App\Controller;

use App\Client\PizzeriaBrest;
use App\Client\PizzeriaParis;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PizzaController extends AbstractController
{
    /**
     * @Route("/")
     *
     * @param PizzeriaBrest $pizzeriaBrest
     * @param PizzeriaParis $pizzeriaParis
     *
     * @return Response
     */
    public function index(PizzeriaBrest $pizzeriaBrest, PizzeriaParis $pizzeriaParis)
    {
        $typesPizzaBrest = ['fromage', 'poivrons', 'fruitsDeMer', 'vegetarienne'];
        $typesPizzaParis = ['fromage'];

        foreach ($typesPizzaBrest as $typeBrest) {
            $pizzeriaBrest->commanderPizza($typeBrest);
        }

        foreach ($typesPizzaParis as $typeParis) {
            $pizzeriaParis->commanderPizza($typeParis);
        }

        return new Response();
    }
}
