<?php

namespace App\Client;

use App\Entity\PizzaInterface;
use App\Entity\PizzaParis\PizzaParisInterface;

class PizzeriaParis extends AbstractPizzeria
{
    protected iterable $pizzas;

    public function __construct(iterable $pizzas)
    {
        $this->pizzas = $pizzas;
    }

    public function creerPizza(string $name): PizzaInterface
    {
        /** @var PizzaParisInterface $pizza */
        foreach ($this->pizzas as $pizza) {
            if ($pizza->isTypeMatch($name)) {
                return $pizza;
            }
        }

        throw new \Exception(sprintf('None pizza found for name %s', $name));
    }
}
