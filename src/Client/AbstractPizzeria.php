<?php

namespace App\Client;

use App\Entity\PizzaInterface;

abstract class AbstractPizzeria
{
    public function commanderPizza(string $type): PizzaInterface
    {
        $pizza = $this->creerPizza($type);

        $pizza->preparer();
        $pizza->cuire();
        $pizza->couper();
        $pizza->emballer();

        return $pizza;
    }

    protected abstract function creerPizza(string $name): PizzaInterface;
}