<?php

namespace App\Entity\PizzaParis;

use App\Entity\AbstractPizza;

class PizzaParisFromage extends AbstractPizza implements PizzaParisInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->setNom("Pizza Paris Fromage");
        $this->setPate("Pâte extra épaisse");
        $this->setSauce("Sauce aux tomates cerise");
        $this->garnitures->add("Parmesan");
        $this->garnitures->add("Mozarella");
        $this->garnitures->add("Comté");
    }

    public function isTypeMatch(string $type): bool
    {
        return $type === 'fromage';
    }
}
