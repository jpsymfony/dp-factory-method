<?php

namespace App\Entity\PizzaBrest;

use App\Entity\AbstractPizza;

class PizzaBrestPoivrons extends AbstractPizza implements PizzaBrestInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->setNom("Pepperoni Brest Pizza");
        $this->setPate("Pâte fine");
        $this->setSauce("Sauce Marinara");
        $this->garnitures->add("Sliced Pepperoni");
        $this->garnitures->add("Sliced Onion");
        $this->garnitures->add("Grated parmesan cheese");
    }

    public function isTypeMatch(string $type): bool
    {
        return $type === 'poivrons';
    }
}
