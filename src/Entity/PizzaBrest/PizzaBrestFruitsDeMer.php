<?php

namespace App\Entity\PizzaBrest;

use App\Entity\AbstractPizza;

class PizzaBrestFruitsDeMer extends AbstractPizza implements PizzaBrestInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->setNom("Pizza Brest Fruits de mer");
        $this->setPate("Pâte fine");
        $this->setSauce("White garlic sauce");
        $this->garnitures->add("Clams");
        $this->garnitures->add("Grated parmesan cheese");
    }

    public function isTypeMatch(string $type): bool
    {
        return $type === 'fruitsDeMer';
    }
}
