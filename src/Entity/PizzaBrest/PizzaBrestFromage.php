<?php

namespace App\Entity\PizzaBrest;

use App\Entity\AbstractPizza;

class PizzaBrestFromage extends AbstractPizza implements PizzaBrestInterface
{
    public function __construct()
    {
        parent::__construct();

        $this->setNom("Pizza Brest Fromage");
        $this->setPate("Pâte fine");
        $this->setSauce("Sauce Marinara");
        $this->garnitures->add("Parmesan");
        $this->garnitures->add("Mozarella");
    }

    public function couper(): void
    {
        echo "<p>Découpage en parts carrées<p/>";
    }

    public function isTypeMatch(string $type): bool
    {
        return $type === 'fromage';
    }
}
